import { Label } from "@/components/ui/label"
import { Input } from "@/components/ui/input"
import { Button } from "@/components/ui/button"
import { Card } from "@/components/ui/card"
import { invoke } from "@tauri-apps/api/tauri"
import { useState } from "react"

export default function UploadPane() {
    const [file, setFile] = useState<File | null>(null);
    const [songTitle, setSongTitle] = useState<string>("");
    const [artistName, setArtistName] = useState<string>("");
    const [albumName, setAlbumName] = useState<string>("");

    const handleFileChange = (event: any) => {
        setFile(event.target.files[0]);
    };

    const uploadTuneFile = async (file: File) => {
        if (!file) return;

        const durationSeconds: number = await new Promise((resolve) => {
            const audio = new Audio();
            audio.src = URL.createObjectURL(file);
            audio.addEventListener('loadedmetadata', function () {
                resolve(audio.duration)
            });
        });

        const fileType = file.name.split('.').pop();
        const hours = Math.floor(durationSeconds / 3600);
        const minutes = Math.floor((durationSeconds % 3600) / 60);
        const seconds = Math.floor(durationSeconds % 60);
        const duration = `${hours}:${minutes}:${seconds}`;

        const reader = new FileReader();
        reader.onload = async (e) => {
            const fileContent = e.target?.result;

            if (fileContent instanceof ArrayBuffer) {
                await invoke("add_tune", {
                    fileContent: Array.from(new Uint8Array(fileContent)),
                    songTitle: songTitle,
                    artistName: artistName,
                    albumName: albumName,
                    duration: duration,
                    fileType: fileType
                })
                    .then((data) => {
                        if (data === "success") {
                            setFile(null);
                            setSongTitle("");
                            setArtistName("");
                            setAlbumName("");
                        }
                    })
                    .catch((error) => {
                        console.error("Error:", error);
                    });
            } else {
                console.error("Failed to read file content.");
            }
        };

        reader.readAsArrayBuffer(file);
        setFile(null);
    };

    return (
        <Card className="mx-auto p-10">
            <div className="max-w-3xl space-y-8 px-4">
                <div className="space-y-2 text-center">
                    <h1 className="text-3xl font-bold">Upload your music</h1>
                    <p className="text-gray-500 dark:text-gray-400">Add new music to your artist profile</p>
                </div>
                <div className="space-y-4">
                    <div className="space-y-2">
                        <Label htmlFor="song-title">Song title</Label>
                        <Input id="song-title" placeholder="Enter the song title"
                            onChange={(e) => setSongTitle(e.target.value)}
                            value={songTitle}
                        />
                    </div>
                    <div className="space-y-2">
                        <Label htmlFor="artist-name">Artist name</Label>
                        <Input id="artist-name" placeholder="Enter the artist name"
                            onChange={(e) => setArtistName(e.target.value)}
                            value={artistName}
                        />
                    </div>
                    <div className="space-y-2">
                        <Label htmlFor="album-name">Album name</Label>
                        <Input id="album-name" placeholder="Enter the album name"
                            onChange={(e) => setAlbumName(e.target.value)}
                            value={albumName}
                        />
                    </div>
                    <div className="space-y-2">
                        <Label htmlFor="song-file">Song file</Label>
                        <Input
                            id="song-file"
                            type="file"
                            onChange={handleFileChange}
                            accept=".mp3,.wav,.flac,.ogg,.m4a,.aac,.wma,.aiff,.alac,.dsd,.dsf,.dff"
                        />
                    </div>
                    <Button
                        onClick={() => file && uploadTuneFile(file)}
                        disabled={!file}
                    >
                        Upload
                    </Button>
                </div>
            </div>
        </Card>
    )
}