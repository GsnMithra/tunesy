"use client";

import SignUpComponent from "@/components/signup";
import { useEffect, useState } from "react";
import { useSession, signIn } from "next-auth/react";
import { redirect, useRouter } from "next/navigation";
import { invoke } from "@tauri-apps/api/tauri";
import zod from "zod";

export default function SignUp() {
    const router = useRouter();
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const { data: session } = useSession();
    const [signedUp, setSignedUp] = useState(false);
    const emailSchema = zod.string().email();
    const passwordSchema = zod.string().min(8);
    const [localStorageToken, setToken] = useState<string | undefined>(
        typeof window !== "undefined" && window.localStorage
            ? localStorage.getItem("token") || undefined
            : undefined,
    );

    useEffect(() => {
        invoke("verify_token", { token: localStorageToken })
            .then((_) => {
                router.push("/");
            })
            .catch((_) => {});

        if (session) redirect("/");
    }, [session, localStorageToken, router]);

    const handleLogin = async (provider: string) => {
        await signIn(provider, { callbackUrl: "/", redirect: false });
    };

    const handleUserCredentials = async () => {
        if (!emailSchema.safeParse(email).success)
            return alert("Please enter a valid email address");
        if (!passwordSchema.safeParse(password).success)
            return alert("Password must be at least 8 characters long");

        await invoke("add_user", {
            email,
            password,
            firstName,
            lastName,
        }).then((_) => {
            setSignedUp(true);
        });
    };

    useEffect(() => {
        if (session || signedUp) redirect("/");
    }, [session, signedUp]);

    return (
        <main className="flex min-h-screen flex-col items-center">
            <SignUpComponent
                firstNameValue={firstName}
                firstNameChange={setFirstName}
                lastNameValue={lastName}
                lastNameChange={setLastName}
                emailValue={email}
                emailChange={setEmail}
                passwordValue={password}
                passwordChange={setPassword}
                login={handleLogin}
                loginCredentials={handleUserCredentials}
            />
        </main>
    );
}
