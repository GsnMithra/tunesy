"use client";

// import React from "react";
import LoginComponent from "@/components/login";
import { useEffect, useState } from "react";
import { useSession, signIn } from "next-auth/react";
import { redirect, useRouter } from "next/navigation";
import { invoke } from "@tauri-apps/api/tauri";
import zod from "zod";

export default function Login() {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const { data: session } = useSession();
    const emailSchema = zod.string().email();
    const router = useRouter();
    const [localStorageToken, setToken] = useState<string | undefined>(
        typeof window !== "undefined" && window.localStorage
            ? localStorage.getItem("token") || undefined
            : undefined,
    );

    useEffect(() => {
        invoke("verify_token", { token: localStorageToken })
            .then((_) => {
                router.push("/");
            })
            .catch((_) => {});

        if (session) redirect("/");
    }, [session, localStorageToken, router]);

    const handleLogin = async (provider: string) => {
        await signIn(provider, { callbackUrl: "/", redirect: false });
    };

    const handleCredentialsLogin = async () => {
        if (!emailSchema.safeParse(email).success)
            return alert("Please enter a valid email");

        await invoke("login_user", {
            email: email,
            password: password,
        })
            .then((token) => {
                if (token) {
                    localStorage.setItem("token", String(token));
                    setToken(String(token));
                }
            })
            .catch((e) => {
                console.error(e);
            });
    };

    return (
        <main className="flex min-h-screen flex-col items-center">
            <LoginComponent
                emailValue={email}
                emailChange={setEmail}
                passwordValue={password}
                passwordChange={setPassword}
                login={handleLogin}
                loginCredentials={handleCredentialsLogin}
            />
        </main>
    );
}
