import "./globals.css";
import type { Metadata } from "next";
import { Work_Sans as FontSans } from "next/font/google";
export const fontSans = FontSans({
    subsets: ["latin"],
    variable: "--font-sans",
});

import SessionProvider from "@/components/sessionprovider";
import { getServerSession } from "next-auth";

export const metadata: Metadata = {
    title: "Tunesy",
    description: "The tunes your ears have been waiting for.",
};

import { cn } from "@/lib/utils";
import { ThemeProvider } from "@/components/theme-provider";

export default async function RootLayout({
    children,
}: Readonly<{
    children: React.ReactNode;
}>) {
    const session = await getServerSession();

    return (
        <html lang="en">
            <body
                className={cn(
                    "min-h-screen bg-background font-sans antialiased",
                    fontSans.variable,
                )}
            >
                <ThemeProvider
                    attribute="class"
                    defaultTheme="system"
                    enableSystem
                    disableTransitionOnChange
                >
                    <SessionProvider session={session}>
                        {children}
                    </SessionProvider>
                </ThemeProvider>
            </body>
        </html>
    );
}
