use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct Tune {
    pub key: String,
    pub song_title: String,
    pub artist_name: String,
    pub album_name: String,
    pub duration: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct JWTPayload {
    pub email: String,
    pub bucket: String,
    pub exp: i64,
}
