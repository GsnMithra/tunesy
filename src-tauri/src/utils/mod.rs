#[path = "../models.rs"]
mod models;
mod subutils;

use aws_config::meta::region::RegionProviderChain;
use aws_config::BehaviorVersion;
use aws_sdk_s3::primitives::ByteStream;
use chrono::{Duration, Utc};
use jsonwebtoken::{decode, encode, Algorithm, DecodingKey, EncodingKey, Header, Validation};
use lazy_static::lazy_static;
use models::{JWTPayload, Tune};
use mongodb::{
    bson::{doc, Document},
    Client, Collection,
};
use std::env;
use subutils::{hash_password, verify_password};

lazy_static! {
    static ref BUCKET: String = env::var("S3_BUCKET_NAME").unwrap().to_string();
    static ref REGION: String = env::var("AWS_REGION").unwrap().to_string();
    static ref DB_URI: String = env::var("DB_URI").unwrap().to_string();
    static ref CLOUDFRONT_URL: String = env::var("CLOUDFRONT_URL").unwrap().to_string();
    static ref JWT_SECRET: String = env::var("JWT_SECRET").unwrap().to_string();
}

#[tauri::command]
pub async fn delete_user(email: String, password: String) -> Result<String, String> {
    let client = Client::with_uri_str(DB_URI.as_str()).await.unwrap();
    let database = client.database("tunesy");

    let collection: Collection<Document> = database.collection("users");
    let users = collection
        .find_one(
            doc! { "email": email.clone(), "password": password.clone() },
            None,
        )
        .await
        .unwrap();

    match users {
        Some(_) => {
            collection
                .delete_one(doc! { "email": email, "password": password }, None)
                .await
                .unwrap();
            Ok("user deleted successfully".to_string())
        }
        None => Err("user not found".to_string()),
    }
}

#[tauri::command]
pub async fn get_cloudfront_url(key: String) -> Result<String, String> {
    let cloudfront_url = CLOUDFRONT_URL.as_str();
    let url = format!("{}/{}", cloudfront_url, key).replace(" ", "%20");
    Ok(url)
}

#[tauri::command]
pub async fn verify_token(token: String) -> Result<String, String> {
    let jwt_secret = JWT_SECRET.clone();
    let token = token.trim_start_matches("Bearer ").trim().to_string();

    match decode::<JWTPayload>(
        &token,
        &DecodingKey::from_secret(jwt_secret.as_ref()),
        &Validation::new(Algorithm::HS256),
    ) {
        Ok(_) => Ok("Token verified".to_string()),
        Err(err) => Err(format!("Token verification failed: {}", err)),
    }
}

#[tauri::command]
pub async fn login_user(email: String, password: String) -> Result<String, String> {
    let client = Client::with_uri_str(DB_URI.as_str()).await.unwrap();
    let database = client.database("tunesy");

    let collection: Collection<Document> = database.collection("users");
    let user = collection
        .find_one(doc! { "email": email.clone() }, None)
        .await
        .unwrap();

    let user = match user {
        Some(user) => user,
        None => return Err("email not found".to_string()),
    };

    let jwt_secret = JWT_SECRET.clone();
    let expiration_time = Utc::now() + Duration::hours(24);

    let claims = JWTPayload {
        email: email.to_owned(),
        bucket: BUCKET.to_owned(),
        exp: expiration_time.timestamp(),
    };

    let token = encode(
        &Header::new(Algorithm::HS256),
        &claims,
        &EncodingKey::from_secret(jwt_secret.as_ref()),
    )
    .unwrap();

    let valid_user = verify_password(
        password.to_string(),
        user.get_str("password").unwrap().to_string(),
    )
    .await;

    match valid_user {
        true => Ok(format!("Bearer {}", token)),
        false => Err("login failed".to_string()),
    }
}

#[allow(non_snake_case)]
#[tauri::command]
pub async fn add_user(
    email: String,
    password: String,
    firstName: String,
    lastName: String,
) -> Result<String, String> {
    let client = Client::with_uri_str(DB_URI.as_str())
        .await
        .map_err(|err| err.to_string())?;
    let database = client.database("tunesy");

    let collection: Collection<Document> = database.collection("users");
    let password = hash_password(password).await;
    let users = collection
        .find_one(doc! { "email": email.clone() }, None)
        .await
        .map_err(|err| err.to_string())?;

    match users {
        Some(_) => Err("email already exists".to_string()),
        None => {
            let user = doc! {
                "firstName": firstName,
                "lastName": lastName,
                "email": email,
                "password": password,
                "liked_songs": []
            };

            if let Err(err) = collection.insert_one(user, None).await {
                return Err(err.to_string());
            }
            Ok("user added successfully".to_string())
        }
    }
}

#[tauri::command]
pub async fn remove_tune(key: String) {
    let region = RegionProviderChain::default_provider().or_else(REGION.as_str());
    let config = aws_config::defaults(BehaviorVersion::latest())
        .region(region)
        .load()
        .await;
    let client = aws_sdk_s3::Client::new(&config);

    client
        .delete_object()
        .bucket(BUCKET.as_str())
        .key(key)
        .send()
        .await
        .expect("error while deleting object");
}

#[allow(non_snake_case)]
#[tauri::command]
pub async fn add_tune(
    fileContent: Vec<u8>,
    songTitle: String,
    artistName: String,
    albumName: String,
    duration: String,
    fileType: String,
) -> Result<String, String> {
    let client = Client::with_uri_str(DB_URI.as_str())
        .await
        .map_err(|err| err.to_string())?;
    let database = client.database("tunesy");

    let collection: Collection<Document> = database.collection("tunes");

    let key = format!("{}{}{}.{}", songTitle, artistName, albumName, fileType);

    let tune = doc! {
        "key": key.clone(),
        "songTitle": songTitle,
        "artistName": artistName,
        "albumName": albumName,
        "duration": duration,
    };

    let tunes = collection
        .find_one(doc! { "key": key.clone() }, None)
        .await
        .map_err(|err| err.to_string())?;

    match tunes {
        Some(_) => return Err("file already exists".to_string()),
        None => {
            if let Err(err) = collection.insert_one(tune, None).await {
                return Err(err.to_string());
            }
        }
    }

    let region = RegionProviderChain::default_provider().or_else(REGION.as_str());
    let config = aws_config::defaults(BehaviorVersion::latest())
        .region(region)
        .load()
        .await;
    let client = aws_sdk_s3::Client::new(&config);

    let mut response = client
        .list_objects_v2()
        .bucket(BUCKET.to_owned())
        .max_keys(10)
        .into_paginator()
        .send();

    let mut object_names: Vec<String> = vec![];
    while let Some(result) = response.next().await {
        match result {
            Ok(output) => {
                for object in output.contents() {
                    object_names.push(object.key().unwrap().to_owned());
                }
            }
            Err(err) => {
                eprintln!("{err:?}")
            }
        }
    }

    if object_names.contains(&key) {
        return Err("file already exists".to_string());
    }

    let body = ByteStream::from(fileContent);
    let response = client
        .put_object()
        .bucket(BUCKET.as_str())
        .content_type("audio/mpeg")
        .key(key.clone())
        .body(body)
        .send()
        .await;

    match response {
        Ok(_) => Ok("success".to_string()),
        Err(err) => Err(err.to_string()),
    }
}

#[tauri::command]
pub async fn get_tunes() -> Result<Vec<Tune>, String> {
    let region = RegionProviderChain::default_provider().or_else(REGION.as_str());
    let config = aws_config::defaults(BehaviorVersion::latest())
        .region(region)
        .load()
        .await;
    let client = aws_sdk_s3::Client::new(&config);

    let db_client = Client::with_uri_str(DB_URI.as_str()).await.unwrap();
    let database = db_client.database("tunesy");
    let collection: mongodb::Collection<Document> = database.collection("tunes");

    let mut response = client
        .list_objects_v2()
        .bucket(BUCKET.to_owned())
        .max_keys(10)
        .into_paginator()
        .send();

    let mut bucket_names: Vec<String> = vec![];
    while let Some(result) = response.next().await {
        match result {
            Ok(output) => {
                for object in output.contents() {
                    bucket_names.push(object.key().unwrap().to_owned());
                }
            }
            Err(err) => {
                eprintln!("{err:?}")
            }
        }
    }

    let mut tunes: Vec<Tune> = vec![];
    for key in bucket_names {
        let tune = collection
            .find_one(doc! { "key": key.clone() }, None)
            .await
            .unwrap()
            .unwrap();

        tunes.push(Tune {
            key: tune.get_str("key").unwrap().to_string(),
            song_title: tune.get_str("songTitle").unwrap().to_string(),
            artist_name: tune.get_str("artistName").unwrap().to_string(),
            album_name: tune.get_str("albumName").unwrap().to_string(),
            duration: tune.get_str("duration").unwrap().to_string(),
        });
    }

    Ok(tunes)
}
